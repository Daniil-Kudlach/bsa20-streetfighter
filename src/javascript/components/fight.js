import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
      // resolve the promise with the winner when fight is over
      const pressedKeys = new Set();
      const options = {
          isWinner: false,
          comboTimeOne: false,
          comboTimeTwo: false,
      }
      const leftHealthIndicator = document.getElementById('left-fighter-indicator');
      const rightHealthIndicator = document.getElementById('right-fighter-indicator');
      const arenaFighterOne = Object.assign({
          isCombo: null,
          isBlock: null,
          indicator: leftHealthIndicator,
          healthDivider: firstFighter.health,
          fighter: firstFighter,
      }, firstFighter);
      const arenaFighterTwo = Object.assign({
          isCombo: null,
          isBlock: null,
          indicator: rightHealthIndicator,
          healthDivider: secondFighter.health,
          fighter: secondFighter,
      }, secondFighter);

      document.addEventListener('keydown', (e) => {
          if (options.isWinner) {
              return;
          }
          let attacker = null,
              defender = null;
          arenaFighterOne.isCombo = false;
          arenaFighterTwo.isCombo = false;
          pressedKeys.add(e.code, true);
          if (!options.comboTimeOne) {
              if (controls.PlayerOneCriticalHitCombination.every(code => pressedKeys.has(code))) {
                  attacker = arenaFighterOne;
                  defender = arenaFighterTwo;
                  options.comboTimeOne = true;
                  attacker.isCombo = true;
                  setTimeout(() => options.comboTimeOne = false, 10000);
              }
          }
          if (!options.comboTimeTwo) {
              if (controls.PlayerTwoCriticalHitCombination.every(code => pressedKeys.has(code))) {
                  attacker = arenaFighterTwo;
                  defender = arenaFighterOne;
                  attacker.isCombo = true;
                  options.comboTimeTwo = true;
                  setTimeout(() => options.comboTimeTwo = false, 10000);
              }
          }
          if (pressedKeys.has(controls.PlayerOneAttack)) {
              attacker = arenaFighterOne;
              defender = arenaFighterTwo;
          }
          if (pressedKeys.has(controls.PlayerTwoAttack)) {
              attacker = arenaFighterTwo;
              defender = arenaFighterOne;
          }
          if (pressedKeys.has(controls.PlayerOneBlock)) {
              arenaFighterOne.isBlock = true;
          }
          if (pressedKeys.has(controls.PlayerTwoBlock)) {
              arenaFighterTwo.isBlock = true;
          }
          if (attacker !== null && !attacker.isBlock) {
            if((!defender.isBlock || attacker.isCombo)){
              defender.health -= attacker.isCombo ? attacker.attack * 2 : getDamage(attacker, defender);
              const indicatorWidth = Math.max(0, (defender.health * 100) / defender.healthDivider);
              defender.indicator.style.width = `${indicatorWidth}%`;
              if (defender.health <= 0) {
                  let winner = attacker.fighter;
                  options.isWinner = true;
                  resolve(winner);
              }
            }
          }
      });
      document.addEventListener('keyup', (e) => {
          e.code == controls.PlayerOneBlock? arenaFighterOne.isBlock = false:null;
          e.code == controls.PlayerTwoBlock? arenaFighterTwo.isBlock = false:null;

          pressedKeys.delete(e.code);
      });
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let hit = getHitPower(attacker);
  let block = getBlockPower(defender);
  let damage = hit - block > 0 ? hit - block : 0;
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random() + 1;
  let power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = Math.random() + 1;
  let power = fighter.defense * dodgeChance;
  return power;
}
