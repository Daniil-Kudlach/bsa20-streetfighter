import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  if(fighter !== undefined){
    const image = createFighterImage(fighter);
    const name = createElement({
      tagName:'span',
      className:'fighter-preview___name'
    });
    const info = createElement({
      tagName:'span',
      className: 'fighter-preview___info',
    });
    name.textContent = `${fighter.name}`;
    info.innerHTML = `<i>Attack:</i> ${fighter.attack}
    <i>Defense:</i> ${fighter.defense}
    <i>Health:</i> ${fighter.health}`;
    fighterElement.append(name,image, info);
  }
return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
