import {showModal} from './modal';
import { createFighterImage } from '../fighterPreview';
export function showWinnerModal(fighter) {
  // call showModal function
  const img = createFighterImage(fighter);
  const modal = {
    title: `${fighter.name.toUpperCase()} WINS!`,
    bodyElement: img,
  };
showModal(modal);
}
